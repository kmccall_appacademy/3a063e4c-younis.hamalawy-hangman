class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    word_length = @referee.pick_secret_word
    @guesser.register_secret_length(word_length)
    @board = '_' * word_length
  end

  def register_secret_length(word_length)
    puts "length of secret word: #{word_length}"
  end

  def take_turn
    @guesser.guess
    checked = @referee.check_guess(@guesser.guess)
    update_board(checked)
    @guesser.handle_response(checked)
  end

  def update_board(guess)
    updated_board = @board.chars.each_index do |idx|
      @board.chars[idx] = guess if @referee.check_guess(guess).include?(idx)
    end
    @board = updated_board.join
  end

  def handle_response(checked)
    puts "Miss" if checked.length == 0
    puts @board if checked.length > 0
  end

end

class HumanPlayer
end

class ComputerPlayer
  attr_accessor :candidate_words
  attr_reader :word

  def initialize(dictionary) #= File.readlines("dictionary.txt"))
    @dictionary = dictionary
  end

  def pick_secret_word
    @word = @dictionary.sample
    @word.length
  end

  def check_guess(guess)
    letter_indicies = []
    @word.chars.each_index do |idx|
      if @word.chars[idx] == guess
        letter_indicies << idx
      end
    end
    letter_indicies
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def guess(board)
    cand_letters = @candidate_words.join.chars - board.join.chars
    hsh = Hash.new(0)
    cand_letters.each do |el|
      hsh[el] += 1
    end
    most_common_letter_value = hsh.values.max
    mod_hash = hsh.reject { |k, v| v == most_common_letter_value }
    selected_key = hsh.keys - mod_hash.keys
    selected_key[0]
  end

  def handle_response(letter, pos)
    length = @candidate_words[0].length
    neg_pos = (0..length).to_a - pos
    pos.each do |idx|
      @candidate_words.select! { |word| word[idx] == letter }
    end
    neg_pos.each do |idx|
      @candidate_words.reject! { |word| word[idx] == letter}
    end
    @candidate_words
  end

end
